<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{
	public function validationDefault(Validator $Validator)
	{
		return $Validator
			->notEmpty('username','A username is require')
			->notEmpty('password', 'A password is require')
			->notEmpty('role','A role is require')
			->add('role','inList',['rule'=>['inList',['admin','author']],
											'message'=>'Please enter a valid role']);
	}
}
?>