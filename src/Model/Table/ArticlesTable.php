<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\Validation\Validator;

//Dat ten ArticlesTable, cake tu hieu table nay se duoc su dung
//trong ArticlesController
class ArticlesTable extends Table
{
	public function initialize(array $config)
	{
	$this->addBehavior('Timestamp');
	}


	public function validationDefault(Validator $validator)
	//kiem tra du lieu khi phuong thuc save duoc goi
	{
		$validator
			->notEmpty('title')
			->requirePresence('title')
			->notEmpty('body')
			->requirePresence('body');
		return $validator;
	}
}

?>