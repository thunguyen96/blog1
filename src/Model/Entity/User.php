<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

class User extends Entity{
	//Làm cho tất cả các trường được gán chung trừ trường khóa chính "id".
	protected $_accessible=[
		'*'=>true,
		'id'=>false
	];
	protected function _setPassword($password)
	{
		if(strlen($password)>0){
			return (new DefaultPasswordHasher)->hash($password);
		}
	}

}

?>